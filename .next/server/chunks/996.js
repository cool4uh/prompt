"use strict";
exports.id = 996;
exports.ids = [996];
exports.modules = {

/***/ 8054:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ WelcomeTemplate)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(625);
/* harmony import */ var _react_email_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(219);
/* harmony import */ var _react_email_components__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_react_email_components__WEBPACK_IMPORTED_MODULE_1__);


function WelcomeTemplate(props) {
    // console.log(props)
    return /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_react_email_components__WEBPACK_IMPORTED_MODULE_1__.Html, {
        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_react_email_components__WEBPACK_IMPORTED_MODULE_1__.Section, {
            style: main,
            children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_react_email_components__WEBPACK_IMPORTED_MODULE_1__.Container, {
                style: container,
                children: [
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_react_email_components__WEBPACK_IMPORTED_MODULE_1__.Text, {
                        style: heading,
                        children: "Welcome to 프롬프트뱅크"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_react_email_components__WEBPACK_IMPORTED_MODULE_1__.Text, {
                        style: paragraph,
                        children: "요청사항 수신 내역"
                    }),
                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_react_email_components__WEBPACK_IMPORTED_MODULE_1__.Hr, {
                        style: hr
                    }),
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_react_email_components__WEBPACK_IMPORTED_MODULE_1__.Text, {
                        style: paragraph,
                        children: [
                            "보낸 사람 :",
                            props[0]
                        ]
                    }),
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_react_email_components__WEBPACK_IMPORTED_MODULE_1__.Text, {
                        style: paragraph,
                        children: [
                            "메일 주소: ",
                            props[1]
                        ]
                    }),
                    /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(_react_email_components__WEBPACK_IMPORTED_MODULE_1__.Text, {
                        style: paragraph,
                        children: [
                            "요청 사항: ",
                            props[2]
                        ]
                    })
                ]
            })
        })
    });
}
// Styles for the email template
const main = {
    backgroundColor: "#f6f9fc",
    padding: "10px 0"
};
const container = {
    margin: "0 auto",
    padding: "20px 0 48px",
    width: "580px",
    border: "1px solid #f0f0f0"
};
const heading = {
    fontSize: "32px",
    lineHeight: "1.3",
    fontWeight: "700",
    color: "#484848"
};
const paragraph = {
    fontSize: "18px",
    lineHeight: "1.4",
    color: "#484848"
};
const hr = {
    borderColor: "#cccccc",
    margin: "20px 0"
};


/***/ }),

/***/ 3242:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "C": () => (/* binding */ sendEmail)
/* harmony export */ });
/* harmony import */ var nodemailer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5184);
/* harmony import */ var nodemailer__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(nodemailer__WEBPACK_IMPORTED_MODULE_0__);

// Replace with your SMTP credentials
const smtpOptions = {
    host: process.env.SMTP_HOST || "smtp.mailtrap.io",
    port: parseInt(process.env.SMTP_PORT || "2525"),
    secure: false,
    auth: {
        user: process.env.SMTP_USER || "user",
        pass: process.env.SMTP_PASSWORD || "password"
    }
};
const sendEmail = async (data)=>{
    const transporter = nodemailer__WEBPACK_IMPORTED_MODULE_0___default().createTransport({
        ...smtpOptions
    });
    return await transporter.sendMail({
        from: process.env.SMTP_FROM_EMAIL,
        ...data
    });
};


/***/ })

};
;