"use strict";
(() => {
var exports = {};
exports.id = 502;
exports.ids = [502];
exports.modules = {

/***/ 219:
/***/ ((module) => {

module.exports = require("@react-email/components");

/***/ }),

/***/ 9068:
/***/ ((module) => {

module.exports = require("@react-email/render");

/***/ }),

/***/ 7640:
/***/ ((module) => {

module.exports = require("next/dist/compiled/react-experimental");

/***/ }),

/***/ 5184:
/***/ ((module) => {

module.exports = require("nodemailer");

/***/ }),

/***/ 4269:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ handler)
/* harmony export */ });
/* harmony import */ var _react_email_render__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(9068);
/* harmony import */ var _react_email_render__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_react_email_render__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _emails_WelcomeTemplate__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(8054);
/* harmony import */ var _src_lib_email__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3242);



async function handler(req, res) {
    alert("send mail");
    let info = await (0,_src_lib_email__WEBPACK_IMPORTED_MODULE_2__/* .sendEmail */ .C)({
        to: "cool4uh@heyoom.com",
        subject: "Welcome to 프롬프트뱅크",
        html: (0,_react_email_render__WEBPACK_IMPORTED_MODULE_0__.render)((0,_emails_WelcomeTemplate__WEBPACK_IMPORTED_MODULE_1__/* ["default"] */ .Z)())
    });
    console.log("Message sent: %s", info.messageId);
    return res.status(200).json({
        message: "Email sent successfully"
    });
}


/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [625,996], () => (__webpack_exec__(4269)));
module.exports = __webpack_exports__;

})();