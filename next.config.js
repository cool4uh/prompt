const config = require("./src/config/config.json");

/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  basePath: config.base_path !== "/" ? config.base_path : "",
  trailingSlash: config.site.trailing_slash,
  compress: false,
  experimental: {
    serverActions: true,
  },
  async redirects() {
    return [
      {
        source: '/api/send-mail',
        destination: '/',
        permanent: true,
      },
    ];
  },
};

module.exports = nextConfig;
