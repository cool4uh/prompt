<h1 align=center>프롬프트뱅크 (Nextjs + Tailwind CSS + TypeScript)</h1>

<p align=center>프롬프트뱅크에 대한 소개를 해드립니다.  다양한 AI엔진의 프롬프트에 대한 기술력으로 필요한 기능을 제공하고 만족스러운 결과를 만들어 드립니다. 저희와 함께 손쉽게 인공지능의 세상으로 함께 나아갈 수 있습니다.</p>

<h2 align="center"> <a target="_blank" href="https://prompt-bank.vercel.app/" rel="nofollow">👀 프롬프트뱅크</a></h2>


## 📌 Key Features

- 👥 Multi-Authors
- 🎯 Similar Posts Suggestion
- 🔍 Search Functionality
- 🌑 Dark Mode
- 🏷️ Tags & Categories
- 🔗 Netlify setting pre-configured
- 📞 Support contact form
- 📱 Fully responsive
- 📝 Write and update content in Markdown / MDX
- 💬 Disqus Comment
- 🔳 Syntax Highlighting

### 📄 15+ Pre-designed Pages

- 🏠 Homepage
- 👤 About
- 📞 Contact
- 👥 Authors
- 👤 Author Single
- 📝 Blog
- 📝 Blog Single
- 🚫 Custom 404
- 💡 Elements
- 📄 Privacy Policy
- 🏷️ Tags
- 🏷️ Tag Single
- 🗂️ Categories
- 🗂️ Category Single
- 🔍 Search

## 🚀 Getting Started

### 📦 Dependencies

- next 13.4+
- node v18+
- npm v9.5+
- tailwind v3.3+

### 👉 Development Command

```
npm run dev
```

### 👉 Build Command

```
npm run build
```

<!-- reporting issue -->

## 🐞 Reporting Issues

We use GitHub Issues as the official bug tracker for this Template. Please Search [existing issues](https://github.com/zeon-studio/nextplate/issues). It’s possible someone has already reported the same problem.
If your problem or idea has not been addressed yet, feel free to [open a new issue](https://github.com/zeon-studio/nextplate/issues).

<!-- licence -->

## 📝 License

**Code License:** Released under the [MIT](https://github.comzeon-studio/nextplate/blob/main/LICENSE) license.

**Image license:** The images are only for demonstration purposes. They have their license, we don't have permission to share those images.

## 💻 Need Custom Development Services?

If you need a custom theme, theme customization, or complete website development services from scratch you can [Hire Us](https://zeon.studio/).
