import {
  Body,
  Button,
  Container,
  Head,
  Hr,
  Html,
  Img,
  Link,
  Preview,
  Row,
  Section,
  Text,
} from '@react-email/components';

export default function WelcomeTemplate(props) {
  // console.log(props)
  return (
    <Html>
      <Section style={main}>
        <Container style={container}>
          <Text style={heading}>Welcome to 프롬프트뱅크</Text>
          <Text style={paragraph}>요청사항 수신 내역</Text>
          <Hr style={hr} />
          <Text style={paragraph}>보낸 사람 :{props[0]}</Text>
          <Text style={paragraph}>메일 주소: {props[1]}</Text>
          <Text style={paragraph}>요청 사항: {props[2]}</Text>
        </Container>
      </Section>
    </Html>
  );
}

// Styles for the email template
const main = {
  backgroundColor: '#f6f9fc',
  padding: '10px 0',
};

const container = {
  margin: "0 auto",
  padding: "20px 0 48px",
  width: "580px",
  border: '1px solid #f0f0f0',

};

const heading = {
  fontSize: "32px",
  lineHeight: "1.3",
  fontWeight: "700",
  color: "#484848",
};

const paragraph = {
  fontSize: "18px",
  lineHeight: "1.4",
  color: "#484848",
};

const hr = {
  borderColor: '#cccccc',
  margin: '20px 0',
};
