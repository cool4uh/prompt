"use client";

import Logo from "@/components/Logo";
import Social from "@/components/Social";
import config from "@/config/config.json";
import menu from "@/config/menu.json";
import social from "@/config/social.json";
import { markdownify } from "@/lib/utils/textConverter";
import Link from "next/link";

const Footer = () => {
  const { copyright } = config.params;

  return (
    <footer className="bg-theme-light dark:bg-darkmode-theme-light">
      <div className="container">
        <div className="row items-center py-10">
          <div className="mb-8 text-center lg:col-3 lg:mb-0 lg:text-left">
            <Logo />
          </div>
          <div className="mb-8 text-center lg:col-6 lg:mb-0">
            <ul>
              {menu.footer.map((menu) => (
                <li className="m-3 inline-block" key={menu.name}>
                  <Link href={menu.url}>{menu.name}</Link>
                </li>
              ))}
            </ul>
          </div>
        </div>
        {/* footer */}
        <div className="m-3 flex-col">
          <div className="md:flex">
            <div className="font-bold">상호명</div>
            <div className="md:ml-3 ml-0">주식회사 아이티혜윰</div>

            <div className="md:flex">
              <div className="md:ml-3 ml-0 font-bold">대표자명</div>
              <div className="md:ml-3 ml-0">김원익</div>
            </div>
          </div>
          <div className="md:flex">
            <div className="font-bold">사업장 주소</div>
            <div className="md:ml-3 ml-0">
              12925 경기도 하남시 미사대로 520, C동 A07-025호 (현대지식산업센터
              한강미사2차)
            </div>
          </div>
          <div className="md:flex">
            <div className="md:flex">
              <div className="font-bold">대표전화</div>
              <div className="md:ml-3 ml-0">02)523-1380</div>
            </div>
            <div className="md:flex">

            <div className="md:ml-3 ml-0 font-bold">사업자 등록번호</div>
            <div className="md:ml-3 ml-0">119-86-14840</div>
            </div>
          </div>
          <div className="md:flex">
            <div className="font-bold">통신판매업 신고번호</div>
            <div className="md:ml-3 ml-0">
              제2022-경기하남-1706호[사업자정보확인]
            </div>
            <div className="md:flex">
              <div className="md:ml-3 ml-0 font-bold">
                개인정보보호책임자
              </div>
              <div className="md:ml-3 ml-0">김원익</div>
            </div>
          </div>
        </div>
        {/* footer */}
      </div>
      <div className="border-t border-border py-7 dark:border-darkmode-border">
        <div className="container text-center text-light dark:text-darkmode-light">
          <p dangerouslySetInnerHTML={markdownify(copyright)} />
        </div>
      </div>
    </footer>
  );
};

export default Footer;

