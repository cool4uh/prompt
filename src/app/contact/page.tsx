import config from "@/config/config.json";
import { getListPage } from "@/lib/contentParser";
import PageHeader from "@/partials/PageHeader";
import SeoMeta from "@/partials/SeoMeta";
import { RegularPage } from "types";

// import { render } from "@react-email/render";
// import WelcomeTemplate from "emails/WelcomeTemplate";
import { sendEmail } from "../../lib/email";
import { redirect } from 'next/navigation';

const Contact = async () => {
  const data: RegularPage = getListPage("pages/contact.md");
  const { frontmatter } = data;
  const { title, description, meta_title, image } = frontmatter;
  const { contact_form_action } = config.params;

  // const formHandler = async (formData: any) => {
  //   "use server";

    // console.log("formData  Reflect.ownKeys(object1)= ",  Reflect.ownKeys(formData)[0]);
    
    // for/in, Object.keys, Object.getOwnPropertyNames 로 못찾음
    // console.log("formData key = ", Object.keys(formData));
    // console.log("formData values = ", Object.values(formData));
    // console.log("formData entries = ", Object.entries(formData));
    // console.log('Object.getOwnPropertyNames = ',Object.getOwnPropertyNames(formData));
    
    // console.log(Object.getOwnPropertySymbols(formData)[0]);
    // console.log(formData)
    // const objectSymbols = Object.getOwnPropertySymbols(formData);
    // const originalArray = formData[objectSymbols[0]];
    // console.log(originalArray)
    // const extractedArray = originalArray.slice(1, 5);
    // console.log(extractedArray)

    // const stringArray = extractedArray.map(obj => {
    //   const keyValuePairs = Object.entries(obj).map(([key, value]) => `${key}: ${value}`);
    //   return keyValuePairs.join(', ');
    // });
    
    // console.log(stringArray);


    // console.log("formData = ", extractedArray.map(obj => JSON.stringify(obj) ) );
    // const sendData = extractedArray.map(obj => JSON.stringify(obj) ) 
    // console.log("sendData = ", sendData);




    // const data = JSON.parse(Buffer.from(formData).toString('utf8'));


    // Extracting name
    // const nameField = formData.find(
    //   (field) => field.name === "name"
    // );
    // const name = nameField ? nameField.value : "";
    // console.log("Name:", name);

    // // Extracting email
    // const emailField = formData[0].find(
    //   (field) => field.name === "mail"
    // );
    // const email = emailField ? emailField.value : "";

    // // Extracting message
    // const messageField = formData[0].find(
    //   (field) => field.name === "message"
    // );
    // const message = messageField ? messageField.value : "";

    // console.log("Email:", email);
    // console.log("Message:", message);

    // const res = await fetch("/api/send", {
    //   method: "POST",
    //   body: f,
    // });

  //   await sendEmail({
  //     to: "cool4uh@gmail.com",
  //     subject: "Welcome to PromptBank",
  //     html: JSON.stringify(stringArray),
  //     // html: render(WelcomeTemplate()),
  //   });

  //   redirect('/');
  // };

  return (
    <>
      <SeoMeta
        title={title}
        meta_title={meta_title}
        description={description}
        image={image}
      />
      <PageHeader title={title} />
      <section className="section-sm">
        <div className="container">
          <div className="row">
            <div className="mx-auto md:col-10 lg:col-6">
              <form action={contact_form_action} method="POST">
              {/* <form action={formHandler}> */}
                <div className="mb-6">
                  <label htmlFor="name" className="form-label">
                    성명 <span className="text-red-500">*</span>
                  </label>
                  <input
                    id="name"
                    name="name"
                    className="form-input"
                    type="text"
                  />
                </div>
                <div className="mb-6">
                  <label htmlFor="mail" className="form-label">
                    이메일 주소 <span className="text-red-500">*</span>
                  </label>
                  <input
                    id="mail"
                    name="mail"
                    className="form-input"
                    type="email"
                  />
                </div>
                <div className="mb-6">
                  <label htmlFor="message" className="form-label">
                    문의사항(요청사항) <span className="text-red-500">*</span>
                  </label>
                  <textarea
                    className="form-input"
                    placeholder="궁금한 사항 또는 요청 사항을 입력해 주세요..."
                    id="message"
                    name="message"
                    rows={8}
                  ></textarea>
                </div>
                {/* <button type="submit" className="btn btn-primary"> */}
                <button type="submit" className="btn btn-primary">
                  등록
                </button>
              </form>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Contact;
