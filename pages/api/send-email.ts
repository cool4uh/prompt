import type { NextApiRequest, NextApiResponse } from "next";
import { render } from "@react-email/render";
import WelcomeTemplate from "../../emails/WelcomeTemplate";
import DropboxResetPasswordEmail from "emails/DropBox";
import GithubAccessTokenEmail from '../../emails/GithubAccessToken';
import { sendEmail } from "../../src/lib/email";

// 이 모듈이 진짜

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const body = await req.body;

  let strArr = Object.keys(body).map(item => body[item]);

  await sendEmail({
    to: "wikim@heyoom.com",
    subject: "Welcome to 프롬프트뱅크",
    // html: render(WelcomeTemplate()),
    html: render(WelcomeTemplate(strArr)),
  });
  // 0607 dev에서는 되나 vercel에서 405오류
  return res.redirect(303, '/');
  // res.end('ok');

  // console.log("Message sent: %s", info.messageId);

  // return res.redirect(200, "/");
  // return res.status(200).redirect("/");
  // return handler;
  // return res.revalidate('/')
  // return res.redirect('/contact');

  // POST를 받을 수 있는 URL
  // return res.redirect(303, '/api/auth/login');

  // return new Response('OK')

}

export async function POST(req: NextApiRequest) {
  const body = await req.body;

  let strArr = Object.keys(body).map(item => body[item]);

  const info =   await sendEmail({
    to: "cool4uh@heyoom.com",
    subject: "Welcome to 프롬프트뱅크",
    // html: render(WelcomeTemplate()),
    html: render(WelcomeTemplate(strArr)),
  });
  console.log('POST request', info)
  return new Response('OK')
}