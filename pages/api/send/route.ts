import type { NextApiRequest, NextApiResponse } from "next";
import { render } from "@react-email/render";
import WelcomeTemplate from "../../../emails/WelcomeTemplate";
import { sendEmail } from "../../../src/lib/email";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  alert("send mail");
  
  let info = await sendEmail({
    to: "cool4uh@heyoom.com",
    subject: "Welcome to 프롬프트뱅크",
    html: render(WelcomeTemplate()),
  });
  console.log("Message sent: %s", info.messageId);

  return res.status(200).json({ message: "Email sent successfully" });
}
